<?php
namespace Bonsai\Controllers;

use Symfony\Component\HttpFoundation\Request;

class MyController extends BaseController
{
   
    public function home() 
    {
        $this->display('home.twig');
    }
    
    public function greet(Request $request)
    {
        $this->display('greeting.twig', ['name' => $request->get('name')]);
    }
}

