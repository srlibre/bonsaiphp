<?php 
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Bonsai\Controllers\MyController;

return function (RoutingConfigurator $routes) {
    $routes->add('home', '/')
        ->controller([MyController::class, 'home']);
    $routes->add('greeting', '/greeting/{name}')
        ->controller([MyController::class, 'greet']);
};